<?php
/**
 * 验证码门面
 * @author: langming
 * @date: 2021/8/24 上午10:31
 */
namespace Langming\Captcha\Facades;

use Illuminate\Support\Facades\Facade;

class Captcha extends Facade
{
    protected static function getFacadeAccessor():string
    {
        return 'Langming\Captcha\Captcha';
    }
}
