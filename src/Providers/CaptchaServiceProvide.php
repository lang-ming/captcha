<?php
/**
 * 验证码服务提供者
 * @author: langming
 * @date: 2021/8/24 上午10:02
 */
namespace Langming\Captcha\Providers;

use Langming\Captcha\Captcha;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

class CaptchaServiceProvide extends ServiceProvider
{
    public function boot()
    {
        if (!file_exists(config_path('captcha.php'))) {
            $this->publishes([
                dirname(__DIR__) . '/../config/captcha.php' => config_path('captcha.php'),
            ], 'captcha-config');
        }
    }

    public function register()
    {
        $this->app->singleton(Captcha::class, function () {
            $config = config('captcha');
            return new Captcha($config);
        });
    }
}
