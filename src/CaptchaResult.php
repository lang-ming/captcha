<?php
/**
 * 验证码返回结果类
 * @author: langming
 * @date: 2021/8/24 上午10:31
 */
namespace Langming\Captcha;

class CaptchaResult
{
    private $captchaByte;  // 验证码图片
    private $captchaMime;  // 验证码类型
    private $captchaCode;  // 验证码内容
    private $createTime;   // 创建时间
    private $config;       // 配置信息

    public function __construct($Byte, $code, $Mime, $config)
    {
        $this->captchaByte = $Byte;
        $this->captchaMime = $Mime;
        $this->captchaCode = $code;
        $this->createTime = time();
        $this->config = $config;
    }

    /**
     * 获取验证码图片
     * @return mixed
     */
    public function getImageByte()
    {
        return $this->captchaByte;
    }

    /**
     * 返回图片Base64字符串
     * @return string
     */
    public function getImageBase64()
    {
        $base64Data = base64_encode($this->captchaByte);
        $Mime = $this->captchaMime;
        return "data:{$Mime};base64,{$base64Data}";
    }

    /**
     * 获取验证码内容
     * @return mixed
     */
    public function getImageCode()
    {
        $captchaCode =  $this->captchaCode;
        return $captchaCode['value'] ?? false;
    }

    /**
     * 获取Mime信息
     */
    public function getImageMime()
    {
        return $this->captchaMime;
    }

    /**
     * 获取配置信息
     * @return mixed
     */
    public function getConfigs()
    {
        return $this->config;
    }
}
