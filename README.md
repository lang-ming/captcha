## 适用于laravel的验证码扩展包
### 基于imagick扩展, imagick是比gd库更高效的图片处理库,性能至少提升25%.
### 环境需求
>* php >=7.3
>* laravel >= 6
>* imagick >= 3.2
### 安装扩展包
>1. composer引入此包 `composer require langming/captcha`
>2. 发布配置文件 `php artisan vendor:publish` 选择tag:captcha-config或者服务提供者(Captcha)
>3. 如果没有找到要发布的配置文件的tag或者服务提供者 执行`composer dump-autoload`
>4. 继续执行步骤2，确保配置文件发布
>5. 修改配置文件，定制验证码。支持三种形式显示（英文、中文、数字运算）
>6. 存储驱动可以选择laravel自带session或者redis
### 使用示例
```
use Langming\Captcha\Facades\Captcha;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        //使用门面生成验证码
        $captcha = Captcha::create(); //返回值为数组
        
        //如果选择session存储则只返回base64格式的图片内容 key:img
        //如果选择redis存储除了返回图片还返回此次存储的key key:codeKey
        //如果选择redis codeKey应在保存表单中（比如隐藏域），随验证码一起提交给验证方法。
        
        //使用门面验证用户输入的验证码
        
        Captcha::verify($userCode); //session存储
        Captcha::verify($userCode, $codeKey); //redis存储 
    }
}
```

