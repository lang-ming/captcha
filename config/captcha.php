<?php
return [
    // 验证码位数
    'length' => 4,
    // 是否使用中文验证码
    'useZh' => false,
    // 是否使用算术验证码
    'math' => true,
    // 验证码字符大小
    'fontSize' => 25,
    // 是否使用混淆曲线
    'useCurve' => true,
    // 是否添加杂点
    'useNoise' => true,
    // 杂色大小
    'fontSizeNoise' => 20,
    // 验证码图片高度
    'imageH' => 60,
    // 验证码图片宽度
    'imageW' => 150,
    // 随机运算符号，支持加法(+)、减法(-)、乘法(*)、除法(/)四则运算
    'operators' => ['+', '-', '*', '/'],
    //缓存
    'cache' => [
        'driver' => 'session',
        'redis' => [
            'connection' => 'default'
        ],
        'prefix' => 'captcha:', //缓存前缀
        'expire' => 180 //有效期
    ],
    //返回格式
    'returnType' => 'byte' //byte |base64
];
